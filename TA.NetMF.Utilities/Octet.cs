// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: Octet.cs  Last modified: 2015-03-24@01:29 by Tim Long

using System;
using System.Text;

namespace TA.NetMF.Utilities
    {
    /// <summary>
    ///     Struct Octet - an immutable representation of an 8 bit byte, with each bit individually addressable.
    /// </summary>
    public struct Octet
        {
        readonly bool[] bits;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Octet" /> struct from an array of at least 8 booleans.
        /// </summary>
        /// <param name="bits">The bits; there must be exactly 8.</param>
        Octet(bool[] bits)
            {
            if (bits.Length < 8) throw new ArgumentException("The array must contain at least 8 items", "bits");
            this.bits = bits;
            }

        /// <summary>
        ///     Gets an Octet with all the bits set to zero.
        /// </summary>
        public static Octet Zero
            {
            get { return zero; }
            }

        /// <summary>
        ///     Gets an Octet set to the maximum value (i.e. all the bits set to one).
        /// </summary>
        public static Octet Max
            {
            get { return max; }
            }

        public bool this[int bit]
            {
            get { return bits[bit]; }
            }

        /// <summary>
        ///     Factory method: create an Octet from an integer.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>Octet.</returns>
        public static Octet FromInt(int source)
            {
            var bits = new bool[8];
            for (var i = 0; i < 8; i++)
                {
                var bit = source & 0x01;
                bits[i] = bit != 0;
                source >>= 1;
                }
            return new Octet(bits);
            }

        /// <summary>
        ///     Factory method: create an Octet from an unisgned integer.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>Octet.</returns>
        public static Octet FromUnsignedInt(uint source)
            {
            return FromInt((int) source);
            }

        /// <summary>
        ///     Returns a new octet with the specified bit number set to the specified value.
        ///     Other bits are duplicated.
        /// </summary>
        /// <param name="bit">The bit number to be modified.</param>
        /// <param name="value">The value of the specified bit number.</param>
        /// <returns>A new octet instance with the specified bit number set to the specified value.</returns>
        public Octet WithBitSetTo(ushort bit, bool value)
            {
            var newBits = new bool[8];
            bits.CopyTo(newBits, 0);
            newBits[bit] = value;
            return new Octet(newBits);
            }

        public override string ToString()
            {
            var builder = new StringBuilder();
            for (int i = 7; i >= 0; i--)
                {
                builder.Append(bits[i] ? '1' : '0');
                builder.Append(' ');
                }
            builder.Length -= 1;
            return builder.ToString();
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="System.UInt32" /> to <see cref="Octet" />.
        /// </summary>
        /// <param name="integer">The integer.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator Octet(uint integer)
            {
            return FromUnsignedInt(integer);
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Octet" /> to <see cref="byte" />.
        /// </summary>
        /// <param name="octet">The octet.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator Byte(Octet octet)
            {
            int sum = 0;
            for (int i = 0; i < 8; i++)
                {
                if (octet[i]) sum += 1 << i;
                }
            return (byte) sum;
            }

        static readonly Octet max = FromInt(0xFF);
        static readonly Octet zero = FromInt(0);
        }
    }