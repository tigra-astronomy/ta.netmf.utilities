﻿// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: MySources.cs  Last modified: 2015-08-17@19:30 by Tim Long

using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities.Samples.Diagnostics
    {
    /// <summary>
    /// You can create debug sources however is convenient, but we've found it is handy to keep them all grouped together in a static class, like this one.
    /// Output formatting needs to know the length of the longest source, so you really need to create them all very early on in your program and making them
    /// all static fields with static initializers is an easy way to achieve that.
    /// </summary>
    internal static class MySources
        {
        internal static Source DemoSource = new Source("Demo"); 
        }
    }