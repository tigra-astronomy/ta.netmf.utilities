// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-03-25@00:45 by Tim Long

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using TA.NetMF.Utilities.ExtensionMethods;
using TA.NetMF.Utilities.I2C;

namespace TA.NetMF.Utilities.Samples.I2CSamples
    {
    public class Program
        {
        /// <summary>
        ///     Demonstrates the use of I2C utility classes.
        /// </summary>
        public static void Main()
            {
            DemonstrateOctet();
            DemonstrateThreadSafeI2CDevice();
            }

        /// <summary>
        ///     Demonstrates use of the <see cref="Octet" /> class.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        static void DemonstrateOctet()
            {
            // Construction
            var octet = Octet.FromInt(0x55); // explicit factory method
            octet = 0x55;           // implicit type conversion from int
            byte b = 0xFE;
            Octet octet1 = b;       // implicit conversion from byte
            uint u = 5;
            octet1 = u;             // implicit conversion from unsigned int
            int output = octet1;    // implicit conversion back to int
            uint output2 = octet1;  // implicit conversion to uint
            byte output3 = octet1;  // implicit conversion to byte
            Debug.Print(octet.ToString()); // prints "0 1 0 1 0 1 0 1" (also shows up in the debugger).
            // Get a copy of the octet with a bit changed (note: octet is immutable, so what you get is a copy).
            var octet2 = octet.WithBitSetTo(0, false);
            // The same as the above, but using a more convenient extension method:
            var octet3 = octet.ClearBit(0); // clears the LSB
            var octet4 = octet.SetBit(7);   // sets the MSB
            // directly test the value of a bit
            if (octet4[7]) Debug.Print("MSB is set");
            Debug.Print("LSB is " + (octet4[0] ? "clear" : "set"));
            // pre-built octet instances, useful for comparing and testing equality
            var allOnes = Octet.Max;
            var allZero = Octet.Zero;
            if (allZero == Octet.Zero)
                {
                // Do something
                }
            }

        /// <summary>
        ///     Demonstrates creating multiple persistent I2C devices (<see cref="Microsoft.SPOT.Hardware.I2CDevice" />
        ///     throws an exception when a second instance is created, because it fails to re-allocate the hardware pins -
        ///     poor design!). Devices can have different configurations, clock speeds and time-outs and can be configured
        ///     to automatically re-try failed transactions. Instances are thread-safe and transactional integrity is
        ///     preserved. Also demonstrates the use of the TransactionBuilder class.
        /// </summary>
        static void DemonstrateThreadSafeI2CDevice()
            {
            // First device is at address 0x31, runs at 10KHz, does not retry failed transactions and uses the default time-out.
            var device1Configuration = new I2CDevice.Configuration(0x31, 10);
            var firstDevice = new ThreadSafeI2CDevice(device1Configuration);
            // Second device is at address 0x38, runs at 20KHz, automatically re-tries up to 3 times and times out after 100ms.
            // Note: the I2CDevice class in MF4.3 throws an exception when a second instance is created (we don't).
            var device2Configuration = new I2CDevice.Configuration(0x38, 20);
            var secondDevice = new ThreadSafeI2CDevice(device2Configuration, 3, 100);
            // The usefulness of the transaction builder is limited in its raw form, but becomes much more apparent
            // when some extension methods have been defined for specific devices. See our SSD1306 display driver
            // for some examples.
            var writeTransaction = TransactionBuilder.WriteTransaction().Append(0xDE, 0xAD, 0xBE, 0xEF);
            var readTransaction = TransactionBuilder.ReadTransaction().AllocateBuffer(100);
            // Execute multiple transactions in one go
            firstDevice.Execute(writeTransaction, readTransaction);
            // Get the results from a read transaction
            var results = readTransaction.ReceivedData; // may be null if the transaction failed.
            // in-line execution of a transaction; re-using an existing builder.
            var bytesSent = secondDevice.Execute(writeTransaction.Clear().Append(0x00));
            // Note that at no time did we have to reset the configuration or dispose any objects.
            }
        }
    }