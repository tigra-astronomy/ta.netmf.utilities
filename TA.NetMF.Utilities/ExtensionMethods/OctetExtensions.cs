// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: OctetExtensions.cs  Last modified: 2015-03-24@01:29 by Tim Long

namespace TA.NetMF.Utilities.ExtensionMethods
    {
    /// <summary>
    ///     Extension methods for <see cref="Octet" />.
    /// </summary>
    public static class OctetExtensions
        {
        /// <summary>
        ///     Returns a copy of <paramref name="source" /> with the specified bitNumber set.
        /// </summary>
        /// <param name="source">The source octet.</param>
        /// <param name="bitNumber">The bit to set.</param>
        /// <returns>A clone of the source octet with the specified bitNumber set.</returns>
        public static Octet SetBit(this Octet source, ushort bitNumber)
            {
            return source.WithBitSetTo(bitNumber, true);
            }

        /// <summary>
        ///     Returns a copy of <paramref name="source" /> with the specified bit clear.
        /// </summary>
        /// <param name="source">The source octet.</param>
        /// <param name="bitNumber">The bit to clear.</param>
        /// <returns>A clone of the source octet with the specified bit clear.</returns>
        public static Octet ClearBit(this Octet source, ushort bitNumber)
            {
            return source.WithBitSetTo(bitNumber, false);
            }
        }
    }