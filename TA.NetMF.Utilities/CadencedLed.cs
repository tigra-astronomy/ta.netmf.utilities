using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace TA.NetMF.Utilities
    {
    /// <summary>
    /// Provides an easy way to flash an LED with various cadence patterns. Actually it doesn't have to be an LED, this can be used to cadence any output port.
    /// </summary>
    public class CadencedLed : IDisposable
        {
        readonly OutputPort port;
        ExtendedTimer cadenceTimer;
        ulong cadencePattern;
        ulong reloadPattern;
        int cadenceIndex;
        const ulong bitMask = 0x8000000000000000;
        object cadenceLock = new object();
        bool disposed;

        public CadencedLed(OutputPort port )
            {
            this.port = port;
            cadencePattern = 0x00000000;    // Default state is OFF.
            var cyclesPerSecond = 0.25;
            var bitsInCadencePattern = 64;
            var bitsPerSecond = bitsInCadencePattern * cyclesPerSecond;
            var secondsPerBit = 1 / bitsPerSecond;
            var timerTick = Timeout.FromSeconds(secondsPerBit);
            cadenceTimer = new ExtendedTimer(CadenceUpdate, null, timerTick.TimeSpan, timerTick.TimeSpan);
            }

        void CadenceUpdate(object ignored)
            {
            if (disposed)
                {
                return;
                }
            lock (cadenceLock)
                unchecked
                    {
                    bool state = (cadencePattern & bitMask) != 0;
                    cadencePattern <<= 1;
                    port.Write(state);
                    if (++cadenceIndex > 63)
                        {
                        cadenceIndex = 0;
                        cadencePattern = reloadPattern;
                        }
                    }
            }

        public void SetCadence(ulong pattern, ulong reload)
            {
            if (disposed)
                {
                throw new ObjectDisposedException();
                }
            lock (cadenceLock)
                {
                cadencePattern = pattern;
                reloadPattern = reload;
                cadenceIndex = 0;
                }
            }

        public void SetCadence(ulong pattern)
            {
            SetCadence(pattern, pattern);
            }

        public void SetReload(ulong pattern)
            {
            if (disposed)
                {
                throw new ObjectDisposedException();
                }
            reloadPattern = pattern;
            }

        /// <summary>
        /// Sets the output state to OFF and stops updates.
        /// Note: does not dispose the output port, you must do that yourself.
        /// </summary>
        public void Dispose()
            {
            if (!disposed)
                {
                cadenceTimer.Dispose();
                cadenceTimer = null;
                disposed = true;
                GC.SuppressFinalize(this);
                }
            }

        ~CadencedLed()
            {
            Dispose();
            }
        }
    }
