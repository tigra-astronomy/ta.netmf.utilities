﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;

namespace TA.NetMF.Utilities.Samples.CadencedLedSample
    {
    public class Program
        {
        public static void Main()
            {
            // Allocate the output ports
            var onboardLed = new OutputPort(Pins.ONBOARD_LED, false);
            var externalLed1 = new OutputPort(Pins.GPIO_PIN_D8, false);
            var externalLed2 = new OutputPort(Pins.GPIO_PIN_D9, false);
            var externalLed3 = new OutputPort(Pins.GPIO_PIN_D10, false);
            var externalLed4 = new OutputPort(Pins.GPIO_PIN_D11, false);
            //while (true)
            //    {
            //    onboardLed.Write(true);
            //    Thread.Sleep(Timeout.FromSeconds(1));
            //    onboardLed.Write(false);
            //    Thread.Sleep(Timeout.FromSeconds(1));
            //    }

            // Create the cadenced LEDs
            var cadencedLed1 = new CadencedLed(onboardLed);
            var cadencedLed2 = new CadencedLed(externalLed1);
            var cadencedLed3 = new CadencedLed(externalLed2);
            var cadencedLed4 = new CadencedLed(externalLed3);
            var cadencedLed5 = new CadencedLed(externalLed4);

            // Start 'em going.

            /*
             * To repeat the same cadence pattern forever, just call SetCadence(pattern).
             * CadencedLed 'plays back' the cadence pattern, then when it gets to the end it is
             * reloaded the reload pattern. If you only specify one parameter, then both the
             * cadence pattern and the reload pattern are set to that pattern, so you get a
             * pattern that repeats forever (or until changed).
             */
            cadencedLed1.SetCadence(Cadence.BlinkMedium);       // Use a predefined patter
            cadencedLed2.SetCadence((ulong)0xF0F0FFFF00000000); // or supply a custom pattern

            /*
             * To start with one cadence, then continue with a different one, specify the reload pattern.
             */
            cadencedLed3.SetCadence(Cadence.BlinkFast, Cadence.Alarm);


            /*
             * By setting the reload pattern to OFF, you can get the LED to flash for one cycle then stop.
             * This is handy if you need to trigger an LED in response to some event, but you don't want
             * to have to remember to turn it off later. You can use this as a sort of 'pulse stretcher'
             * where the trigger would be too short to see. One example of how to use this is as an indication
             * of data flow over a communications channel (serial port, network, etc). Each time you receive
             * some data, trigger the LED, then just let it turn itself off.
             * 
             * To demonstrate the effect, we'll use timers to trigger two LEDs with different trigger periods.
             */

            var slowTrigger = Timeout.FromSeconds(11);
            var fastTrigger = Timeout.FromSeconds(6);
            var timer1 = new Timer(TriggerLed, cadencedLed4, fastTrigger.TimeSpan, fastTrigger.TimeSpan);
            var timer2 = new Timer(TriggerLed, cadencedLed5, slowTrigger.TimeSpan, slowTrigger.TimeSpan);

            // Put this thread to sleep and let the timers do their magic.
            Thread.Sleep(System.Threading.Timeout.Infinite);
            }

        static void TriggerLed(object state)
            {
            var led = state as CadencedLed;
            if (led == null) return;
            led.SetCadence(Cadence.Strobe, Cadence.SteadyOff);
            }
        }
    }
