﻿// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: Network.cs  Last modified: 2015-08-18@01:44 by Tim Long

using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Net.NetworkInformation;
using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities
    {
    public static class Network
        {
        static readonly ManualResetEvent networkAvailableEvent = new ManualResetEvent(false);
        static bool subscribedToAvailabilityChanged;

        /// <summary>
        ///     Waits for the whole network stack to become available.
        ///     On return from this method:
        ///     The link layer driver will have enabled the physical link and have a valid MAC address.
        ///     The network layer will be available and will have at least one valid IP address.
        /// </summary>
        /// <remarks>
        ///     NOTE: If the network doesn't initialize, then this method will never return.
        ///     If you need to continue without a network connection then you can suscribe to
        ///     <see cref="NetworkChange.NetworkAvailabilityChanged" /> and <see cref="NetworkChange.NetworkAddressChanged" />
        ///     and handle the notifications yourself.
        /// </remarks>
        /// <seealso cref="NetworkChange" />
        /// <seealso cref="Network" />
        /// <seealso cref="WaitForLinkLayer" />
        /// <seealso cref="WaitForNetworkLayer" />
        public static void WaitForNetworkAvailable()
            {
            SubscribeToNetworkChangedEvents();
            WaitForLinkLayer();
            WaitForNetworkLayer();
            }

        /// <summary>
        ///     Waits for the network link layer to become available.
        ///     On return from this method, there will be a valid MAC address and the physical link will be active.
        ///     The network layer may not yet be ready and there may not be an IP address.
        /// </summary>
        /// <seealso cref="WaitForNetworkLayer" />
        /// <seealso cref="WaitForNetworkAvailable" />
        public static void WaitForLinkLayer()
            {
            if (!subscribedToAvailabilityChanged)
                {
                subscribedToAvailabilityChanged = true;
                NetworkChange.NetworkAvailabilityChanged += SignalNetworkAvailabilityChanged;
                }
            Dbg.Trace("Waiting for link layer", Source.Network);
            networkAvailableEvent.WaitOne();
            }

        static void SignalNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
            {
            if (e.IsAvailable)
                {
                networkAvailableEvent.Set();
                }
            else
                {
                networkAvailableEvent.Reset();
                }
            }

        static void SubscribeToNetworkChangedEvents()
            {
            NetworkChange.NetworkAvailabilityChanged += LogNetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged += LogNetworkAddressChanged;
            }

        static void UnsubscribeFromNetworkChangedEvents()
            {
            NetworkChange.NetworkAvailabilityChanged -= LogNetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged -= LogNetworkAddressChanged;
            }

        static void LogNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
            {
            if (e.IsAvailable)
                {
                Dbg.Trace("Link layer available", Source.Network);
                networkAvailableEvent.Set();
                }
            else
                {
                networkAvailableEvent.Reset();
                Dbg.Trace("Link layer down", Source.Network);
                }
            }

        static void LogNetworkAddressChanged(object sender, EventArgs ea)
            {
            Dbg.Trace("Network address changed:", Source.Network);
            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            if (interfaces.Length == 0)
                {
                Dbg.Trace("No interfaces detected", Source.Network);
                }
            foreach (NetworkInterface nic in interfaces)
                {
                Dbg.Trace(nic.PhysicalAddress.AsHexString(":")
                          + " "
                          + nic.IPAddress
                          + " ("
                          + (nic.IsDhcpEnabled ? "DHCP" : "static")
                          + ")",
                    Source.Network);
                }
            }

        /// <summary>
        ///     Waits for the network layer to become available and configured with a valid IP address.
        ///     Assumes that the link layer network handler is already available.
        /// </summary>
        /// <seealso cref="WaitForLinkLayer" />
        /// <seealso cref="WaitForNetworkAvailable" />
        public static void WaitForNetworkLayer()
            {
            NetworkInterface[] networkInterfaces;
            Dbg.Trace("Waiting for network layer", Source.Network);
            do
                {
                Thread.Sleep(100);
                networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                } while (networkInterfaces[0].IPAddress == "0.0.0.0");

            Dbg.Trace("Found " + networkInterfaces.Length + " network interfaces - details as follows...",
                Source.Network);
            for (int i = 0; i < networkInterfaces.Length; i++)
                {
                var nic = networkInterfaces[i];
                Dbg.Trace("Network interface: " + i, Source.Network);
                Dbg.Trace("  Interface type: " + nic.NetworkInterfaceType, Source.Network);
                Dbg.Trace("  IPv4 address: " + nic.IPAddress, Source.Network);
                Dbg.Trace("  Default gateway: " + nic.GatewayAddress, Source.Network);
                Dbg.Trace("  Subnet mask: " + nic.SubnetMask, Source.Network);

                var dnsServers = nic.DnsAddresses;
                Dbg.Trace("  DNS servers:", Source.Network);
                foreach (var dnsServer in dnsServers)
                    {
                    Dbg.Trace("    " + dnsServer, Source.Network);
                    }
                }
            }
        }
    }