﻿// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-03-24@01:29 by Tim Long

using System.Reflection;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Tigra Astronomy")]
[assembly: AssemblyProduct("TA.NetMF.Utilities")]
[assembly: AssemblyCopyright("Copyright © 2015 Tigra Astronomy, all rights reserved")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.3.*")]
[assembly: AssemblyFileVersion("0.3.*")]
[assembly: AssemblyInformationalVersion("0.3 uncontrolled build")]