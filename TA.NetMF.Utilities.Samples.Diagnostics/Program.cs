﻿// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-08-17@19:42 by Tim Long

using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities.Samples.Diagnostics
    {
    public class Program
        {
        public static void Main()
            {
            Dbg.Trace("Starting diagnostics demo", MySources.DemoSource);
            for (int i = 0; i < 10; i++)
                {
                Dbg.Trace("i = " + i, MySources.DemoSource);
                }

            // Some sources used in the Utilities namespace are predefined.
            Dbg.Trace("An unspecified source", Source.Unspecified);

            // Sources can be disabled, which mutes their output.
            Dbg.SetSource(Source.Unspecified, false);
            Dbg.Trace("This line should not appear in the output", Source.Unspecified);
            Dbg.SetSource(Source.Unspecified, true);
            Dbg.Trace("Enabled sources produce output", Source.Unspecified);

            // Format byte array as hexadecimal: AsHexString extension method
            var bytes = new byte[] {5, 9, 10, 15, 16, 127, 128, 255};
            Dbg.Trace("Output bytes in hexadecimal: " + bytes.AsHexString(), MySources.DemoSource);
            Dbg.Trace("Specify the separator: " + bytes.AsHexString(", "), MySources.DemoSource);
            }
        }
    }