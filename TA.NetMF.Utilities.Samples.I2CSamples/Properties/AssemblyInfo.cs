// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-03-25@16:24 by Tim Long

using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("TA.NetMF.Utilities.Samples.I2CSamples")]
[assembly: AssemblyDescription("Sample code demonstrating usage of the utility classes")]