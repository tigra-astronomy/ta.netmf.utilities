﻿// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-08-18@02:44 by Tim Long

using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities.Samples
    {
    /// <summary>
    ///     <para>
    ///         This program demonstrates the use of our <see cref="Watchdog" /> timer utility. However, you should be
    ///         aware that this isn't a 'proper' watchdog. A 'proper' watchdog should be implemented in hardware, and
    ///         there should be no way to stop it once it has started. Nevertheless, this utility class might get you
    ///         out of hot water in some situations, as long as the <see cref="Timer" /> clas is able to schedule its
    ///         callbacks. We've used this class to great effect in situations where the network stack appears to become
    ///         deadlocked.
    ///     </para>
    ///     <para>
    ///         If the watchdog timer becomes a nuisance during debugging, then you have several options:
    ///         1. Use #if directives to avoid starting the timer
    ///         2. Use #if directives to set the timeout to a really long time. 
    ///         3. Pull out the wire to the /RESET pin so the watchdog can't drive it low
    ///         4. Wire it up to an LED instead, so you still get a visual indication.
    ///         For this demo, we use 2 seconds in the Release build and 5 seconds in the Debug build, just to show the principle.
    ///     </para>
    /// </summary>
    public class Program
        {
        public static void Main()
            {
            /*
             * First, choose an output port. This will be driven low if the watchdog expires.
             * Connect your chosen output to the /RESET pin, or for testing you can wire it up to an LED.
             * Setting the port can only be done once and you should do it right at the start of your Main() method.
             */
            var port = new OutputPort(Pins.GPIO_PIN_D8, true);
            Watchdog.ConfigureResetOutput(port);

            /*
             * Now you can create a Watchdog instance. You can create as many as you need, each with different timeouts,
             * but they all share the same output pin. The name argument is only used to generate diagnostic output.
             */

            var watchdogInterval = Timeout.FromSeconds(5);
            var watchdog = new Watchdog(watchdogInterval, "Demo Watchdog");

            /*
             * The timer hasn't started yet, so we can wait a few seconds and nothing will happen.
             */
            var wait = watchdogInterval + Timeout.FromSeconds(2);
            Dbg.Trace("Waiting " + wait + " to show that nothing will happen", Source.Unspecified);
            Thread.Sleep(wait);

            /*
             * Now we'll enable the watchdog timer. We'll then go into a wait loop that gets steadily longer
             * until eventually the watchdog will time out and reboot the system.
             */

            var increment = Timeout.FromSeconds(1);
            var waitTime = Timeout.FromSeconds(1);
            watchdog.Start();

            while (true)
                {
                Dbg.Trace("Waiting " + waitTime, Source.Unspecified);
                Thread.Sleep(waitTime);
                watchdog.Restart();
                waitTime += increment;
                }
            }
        }
    }