// This file is part of the TA.NetMF.WeatherServer project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: Cadence.cs  Last modified: 2015-07-31@08:47 by Tim Long

namespace TA.NetMF.Utilities
    {
    /// <summary>
    /// Pre-defined cadence patterns for <see cref="CadencedLed"/>.
    /// </summary>
    public static class Cadence
        {
        public static readonly ulong SteadyOn = 0xFFFFFFFFFFFFFFFF;

        public static readonly ulong SteadyOff = 0x0;

        public static readonly ulong BlinkSlow = 0xFFFFFFFF00000000;

        public static readonly ulong BlinkMedium = 0xFFFF0000FFFF0000;

        public static readonly ulong BlinkFast = 0xF0F0F0F0F0F0F0F0;

        public static readonly ulong Alarm = 0x8000800080008000;

        public static readonly ulong Strobe = 0xAAAAAAAAAAAAAAAA;

        public static readonly ulong Wink = 0xFFFFFFFFFFFFFFFC;
        }
    }