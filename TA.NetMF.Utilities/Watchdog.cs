// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: Watchdog.cs  Last modified: 2015-08-18@02:32 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;
using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities
    {
    /// <summary>
    ///     Implements a watchdog timer that can be used to reset the target system in the event of a hang. Uses output port
    ///     D13 by default, but this can be overridden by calling <see cref="ConfigureResetOutput" />. All instances use the
    ///     same output pin.
    /// </summary>
    /// <remarks>
    ///     Watchdog timers really need to use a hardware timer that cannot be disabled once it has been started. The ARM
    ///     chip on Netduino has such a timer built in, but unfortunately the firmware doesn't expose it. There is a
    ///     watchdog class built into the Micro Framework, but it doesn't appear to work as expected. This class will never
    ///     take the place of a proper hardware-based watchdog, but it might just save your bacon if you're lucky. A
    ///     <see cref="Timer" /> is used and in our experience, timer callbacks are pretty reliable even in the face of
    ///     other
    ///     parts of the firmware being hung or deadlocked, so this is a gnat's whisker better than nothing. Nevertheless,
    ///     we highly recommend that you consider a hardware solution.
    /// </remarks>
    public class Watchdog : IDisposable
        {
        //Digital output D13 should be strapped to /RESET
        static OutputPort reset;

        /// <summary>
        ///     Gets the guard time.
        /// </summary>
        /// <value>The guard time.</value>
        public Timeout GuardTime { get; private set; }

        Timer timer;
        bool disposed;

        /// <summary>
        ///     Configures the reset output pin. The selected port will be driven low if the watchdog expires. The port
        ///     should be connected to /RESET. D13 is used by default unless changed by calling this method. This should
        ///     be done at most once and as soon as possible after start-up.
        /// </summary>
        /// <example>
        ///     Watchdog.ConfigureResetOutput(new OutputPort(Pins.GPIO_PIN_D13, true));
        /// </example>
        /// <remarks>
        ///     Be sure to set the initial state of the output port to
        ///     <c>true</c>, otherwise there is a risk that your target system may be immediately reset.
        /// </remarks>
        /// <param name="resetPin">The output port to use as the reset pin.</param>
        public static void ConfigureResetOutput(OutputPort resetPin)
            {
            if (reset != null)
                {
                reset.Dispose();
                }
            reset = resetPin;
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Watchdog" /> class and sets the guardTime guard interval.
        /// </summary>
        /// <param name="guardTime">
        ///     The guard interval, after which the target system may be rebooted, unless
        ///     <see cref="Restart" /> is called before the interval expires.
        /// </param>
        /// <param name="name">The display name (for diagnostics).</param>
        public Watchdog(Timeout guardTime, string name)
            {
            GuardTime = guardTime;
            Name = name;
            Dbg.Trace("Watchdog timer '" + Name + "' created with guard time " + GuardTime, Source.Watchdog);
            }

        /// <summary>
        ///     Starts the watchdog timer. Only the first invocation has any effect, subsequent calls are ignored. Once
        ///     started, the timer cannot be stopped.
        /// </summary>
        public void Start()
            {
            if (reset == null) throw new InvalidOperationException("Output port has not been configured");
            if (timer == null)
                {
                timer = new Timer(Expired, null, GuardTime.TimeSpan, GuardTime.TimeSpan);
                Dbg.Trace("Watchdog '" + Name + "' started", Source.Watchdog);
                }
            }

        /// <summary>
        ///     Restarts (clears, resets) the watchdog timer, and prevents the target system from being rebooted for at least
        ///     <see cref="GuardTime" />
        ///     Restart must be called with a frequency faster than the guardTime period, or the target system will be rebooted.
        /// </summary>
        public void Restart()
            {
            if (disposed)
                throw new ObjectDisposedException();
            if (timer == null)
                throw new InvalidOperationException("Watchdog has not been started");
            timer.Change(GuardTime.TimeSpan, GuardTime.TimeSpan);
            Dbg.Trace("Watchdog '" + Name + "' restarted", Source.Watchdog);
            }

        void Expired(object ignoredState)
            {
            Dbg.Trace("Watchdog '" + Name + "' EXPIRED", Source.Watchdog);
            if (disposed)
                {
                Dbg.Trace("Watchdog was disposed, not rebooting", Source.Watchdog);
                }
            RebootTargetSystem();
            }

        void RebootTargetSystem()
            {
            reset.Write(false); // hardware reset
            }

        string Name { get; set; }

        public void Dispose()
            {
            if (disposed) return;
            disposed = true;
            timer.Dispose();
            }
        }
    }