// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: TransactionBuilder.cs  Last modified: 2015-03-24@22:17 by Tim Long

using System;
using System.Collections;
using Microsoft.SPOT.Hardware;
//using JetBrains.Annotations;

namespace TA.NetMF.Utilities.I2C
    {
    /// <summary>
    ///     Class TransactionBuilder. A class for building I2C transactions using a fluent API.
    /// </summary>
    /// <remarks>
    ///     A relatively naive storage implementation is used, but as the amount of data being handled is relatively
    ///     small then this shouldn't be a problem in practice. The storage mechanism is fully encapsulated and hidden,
    ///     so in future the storage mechanism could be improved along the lines of <c>StringBuilder</c> class without
    ///     breaking any clients.
    /// </remarks>
    public class TransactionBuilder : IWriteTransactionBuilder, IReadTransactionBuilder
        {
        readonly ArrayList buffer;

        protected TransactionTypes TransactionType { get; set; }

        protected enum TransactionTypes
            {
            Read,
            Write
            }

        /// <summary>
        ///     Prevents a default instance of the <see cref="TransactionBuilder" /> class from being created. Instance
        ///     must be created using one of the static factory methods, <see cref="WriteTransaction" /> or
        ///     <see cref="ReadTransaction" />.
        /// </summary>
        protected TransactionBuilder()
            {
            buffer = new ArrayList();
            }

        public static IReadTransactionBuilder ReadTransaction()
            {
            return new TransactionBuilder {TransactionType = TransactionTypes.Read};
            }

        public static IWriteTransactionBuilder WriteTransaction()
            {
            return new TransactionBuilder {TransactionType = TransactionTypes.Write};
            }


        public IWriteTransactionBuilder Append(params byte[] bytes)
            {
            foreach (var b in bytes)
                {
                buffer.Add(b);
                }
            return this;
            }

        public IWriteTransactionBuilder Append(params int[] integers)
            {
            foreach (var integer in integers)
                {
                buffer.Add((byte) integer);
                }
            return this;
            }

        /// <summary>
        ///     Converts the accumulated data to an instance of <see cref="I2CDevice.I2CTransaction" />
        /// </summary>
        /// <returns>Microsoft.SPOT.Hardware.I2CDevice.I2CTransaction.</returns>
        public I2CDevice.I2CTransaction ToI2CTransaction()
            {
            I2CDevice.I2CTransaction transaction;
            switch (TransactionType)
                {
                    case TransactionTypes.Read:
                        ReceivedData = (byte[]) buffer.ToArray(typeof (byte));
                        transaction = I2CDevice.CreateReadTransaction(ReceivedData);
                        break;
                    case TransactionTypes.Write:
                        transaction = I2CDevice.CreateWriteTransaction((byte[]) buffer.ToArray(typeof (byte)));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            return transaction;
            }


        /// <summary>
        ///     Clears the internal buffer so this instance can be used to build a new transaction.
        /// </summary>
        /// <returns>Returns the source <see cref="IWriteTransactionBuilder" /> instance.</returns>
        public IWriteTransactionBuilder Clear()
            {
            buffer.Clear();
            return this;
            }

        /// <summary>
        ///     Allocates space in the read buffer large enough to receive the specified number of bytes.
        /// </summary>
        /// <param name="numberOfBytes">The number of bytes.</param>
        /// <returns>IWriteTransactionBuilder.</returns>
        public IReadTransactionBuilder AllocateBuffer(int numberOfBytes)
            {
            for (int i = 0; i < numberOfBytes; i++)
                {
                Append(0x00);
                }
            return this;
            }

        /// <summary>
        ///     Gets the received data after the transaction has been executed.
        /// </summary>
        /// <value>The raw data as received from the I2C bus. Always check for <see langword="null" />!</value>
        public byte[] ReceivedData { get; set; }
        }
    }