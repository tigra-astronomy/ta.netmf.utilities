// This file is part of the TA.NetMF.Utilities project
// 
// Copyright © 2015 Tigra Astronomy., all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-03-25@16:22 by Tim Long

using System.Reflection;

[assembly: AssemblyTitle("TA.NetMF.Utilities")]
[assembly: AssemblyDescription("Utility classes and extension methods for Netduino and .Net Micro Framework")]