// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: IWriteTransactionBuilder.cs  Last modified: 2015-03-24@21:27 by Tim Long

namespace TA.NetMF.Utilities.I2C
    {
    public interface IWriteTransactionBuilder : ITransactionBuilder
        {
        IWriteTransactionBuilder Append(params byte[] bytes);

        IWriteTransactionBuilder Append(params int[] integers);
        }
    }