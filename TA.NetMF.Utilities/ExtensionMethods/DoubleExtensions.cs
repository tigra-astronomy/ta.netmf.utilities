// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: DoubleExtensions.cs  Last modified: 2015-03-24@01:29 by Tim Long

using System;

namespace TA.NetMF.Utilities.ExtensionMethods
    {
    /// <summary>
    ///     Extension methods for <see cref="double" />.
    /// </summary>
    public static class DoubleExtensions
        {
        /// <summary>
        ///     Constrains (or clips) the value to the specified limits. The limits may be in any order.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="limit1">The minimum.</param>
        /// <param name="limit2">The maximum.</param>
        /// <returns>System.Double.</returns>
        public static double ConstrainToLimits(this double value, double limit1, double limit2)
            {
            double max, min;
            if (limit1 > limit2)
                {
                max = limit1;
                min = limit2;
                }
            else
                {
                max = limit2;
                min = limit1;
                }
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
            }

        public static Boolean IsWithinAGnatsWhiskerOf(this double lhs, double rhs, double tolerance = 1E-8)
            {
            var positiveDifference = Math.Abs(lhs - rhs);
            return positiveDifference < tolerance;
            }
        }
    }