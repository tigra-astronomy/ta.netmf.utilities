// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: DiagnosticExtensions.cs  Last modified: 2015-08-17@19:55 by Tim Long

using System.Text;

namespace TA.NetMF.Utilities.Diagnostics
    {
    public static class DiagnosticExtensions
        {
        /// <summary>
        ///     formats a byte array as a hexadecimal string.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="separator">
        ///     The separator character to be inserted between bytes. Optional; defaults to a
        ///     hyphen. For no separator, specify <see cref="string.Empty" />.
        /// </param>
        /// <returns>A new string containing the bytes formatted as hexadecimal.</returns>
        public static string AsHexString(this byte[] bytes, string separator = "-")
            {
            var builder = new StringBuilder();
            foreach (var b in bytes)
                {
                builder.Append(b.ToString("x2")).Append(separator);
                }
            if (builder.Length > 2)
                {
                builder.Length--; // Remove trailing hyphen
                }
            return builder.ToString();
            }
        }
    }