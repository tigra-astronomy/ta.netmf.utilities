﻿using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Net.NetworkInformation;
using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities.Samples
    {
    public class Program
        {
        public static void Main()
            {
            Network.WaitForNetworkAvailable();
            SetSystemClock();
            Dbg.Trace("Clock set to "+DateTime.UtcNow, Source.Unspecified);
            Thread.Sleep(System.Threading.Timeout.Infinite);
            }

        /// <summary>
        /// Gets the time from the default NTP time server and uses the returned time to set the system clock.
        /// </summary>
        /// <remarks>
        /// In theory, you could convert the UTC time to local time using <c>var local = utc.ToLocalTime();</c>
        /// However, I'm not sure how Netduino handles time zone offsets, so I prefer to just keep everything as UTC.
        /// </remarks>
        static void SetSystemClock()
            {
            var utc = NetworkTime.GetNetworkTimeUtc();
            Utility.SetLocalTime(utc);
            Dbg.Trace("Set system time to: " + utc.ToString() + " UTC", Source.Unspecified);
            }



        }
    }
