// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: Dbg.cs  Last modified: 2015-08-17@19:29 by Tim Long

using System.Collections;
using System.Diagnostics;
using System.Text;
using Microsoft.SPOT;

namespace TA.NetMF.Utilities.Diagnostics
    {
    /// <summary>
    ///     Builds upon <see cref="Debug.Print" /> to provide more consistent and better looking diagnostic output and finer
    ///     grained control.
    ///     Output can be filtered based on its source; sources can also be thought of as categories.
    ///     New sources are automatically enabled by default, this is controlled by the <see cref="AutoEnable" /> property,
    ///     which defaults to <c>true</c>.
    ///     Sources can be enabled or muted manually by calling <see cref="SetSource" />.
    ///     <seealso cref="Source" />
    /// </summary>
    public static class Dbg
        {
        static readonly IDictionary traceSwitches = new Hashtable();

        public static bool AutoEnable { get; set; }

        static Dbg()
            {
            AutoEnable = true;
            }

        public static void Trace(string message, Source source)
            {
            if (!traceSwitches.Contains(source))
                {
                if (AutoEnable)
                    {
                    traceSwitches.Add(source, true);
                    }
                else
                    return;
                }
            var enabled = (bool) traceSwitches[source];
            if (!enabled)
                return;
            var builder = new StringBuilder();
            var sourceLength = source.Name.Length;
            builder.Append('[');
            builder.Append(source.Name);
            if (sourceLength < Source.LongestSource)
                builder.Append(new string(' ', Source.LongestSource - sourceLength));
            builder.Append(']');
            builder.Append(' ');
            builder.Append(message);
            Debug.Print(builder.ToString());
            }

        /// <summary>
        ///     Provides manual control of debug sources. AN enabled source will produce output; a disabled source is muted.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="enabled">if set to <c>true</c> then output from this source is enabled; otherwise is is disabled.</param>
        [Conditional("DEBUG")]
        public static void SetSource(Source source, bool enabled)
            {
            traceSwitches[source] = enabled; // Hashtable allows overwrite
            }
        }
    }