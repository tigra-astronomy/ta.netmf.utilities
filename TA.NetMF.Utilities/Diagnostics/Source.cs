// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: Source.cs  Last modified: 2015-08-17@19:17 by Tim Long

namespace TA.NetMF.Utilities.Diagnostics
    {
    /// <summary>
    ///     Class Source. An immutable type representing a source of diagnostic trace data.
    ///     Instances of this class can only be obtained using the static readonly fields containing
    ///     pre-built instances.
    /// </summary>
    public sealed class Source
        {
        readonly string name;
        public static readonly Source Unspecified = new Source("Unspecified");
        public static readonly Source NetTime = new Source("Network Time");
        public static readonly Source Network = new Source("Network");
        public static readonly Source Watchdog = new Source("Watchdog");

        public Source(string name)
            {
            this.name = name;
            var length = name.Length;
            if (length > LongestSource)
                LongestSource = length;
            }

        public string Name
            {
            get { return name; }
            }

        /// <summary>
        ///     Gets the length in characters of the longest source name.
        /// </summary>
        /// <value>The length of the longest source.</value>
        public static int LongestSource { get; private set; }
        }
    }