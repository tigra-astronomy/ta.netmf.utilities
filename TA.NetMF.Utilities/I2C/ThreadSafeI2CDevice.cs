// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: ThreadSafeI2CDevice.cs  Last modified: 2015-03-24@21:54 by Tim Long

using System.IO;
using Microsoft.SPOT.Hardware;

namespace TA.NetMF.Utilities.I2C
    {
    /// <summary>
    ///     Class ThreadSafeI2CDevice.
    ///     Acts as a wrapper around <see cref="I2CDevice" /> and makes it easier to create and manage multiple I2C
    ///     devices while maintaining thread safety and transactional integrity.
    /// </summary>
    public class ThreadSafeI2CDevice
        {
        readonly I2CDevice.Configuration configuration;
        static I2CDevice iicBus;
        static readonly object busMutex = new object(); // MUTEX

        /// <summary>
        ///     Initializes a new instance of the <see cref="ThreadSafeI2CDevice" /> class.
        /// </summary>
        /// <param name="configuration">The I2C configuration for thsi device.</param>
        /// <param name="retryLimit">The retry limit; optional, defaults to 0 (no automatic re-tries).</param>
        /// <param name="timeout">The transaction timeout in milliseconds; optional, defaults to 1000 ms.</param>
        public ThreadSafeI2CDevice(I2CDevice.Configuration configuration, int retryLimit = 0, int timeout = 1000)
            {
            RetryLimit = retryLimit;
            Timeout = timeout;
            this.configuration = configuration;
            }

        /// <summary>
        ///     Gets or sets the number of times to retry a failed transaction.
        ///     A failed transaction (NAKed or timed out) will be re-tried up to this many times before an exception is thrown.
        /// </summary>
        /// <value>The retry limit; set to 0 to disable automatic retries.</value>
        public int RetryLimit { get; set; }

        /// <summary>
        ///     Gets or sets the transaction timeout.
        ///     If <see cref="RetryLimit" /> is non-zero, timed out transactions will be re-tried.
        /// </summary>
        /// <value>The timeout.</value>
        public int Timeout { get; set; }

        /// <summary>
        ///     Executes the specified transactions on the I2C bus, ensuring that no other thread can interrupt the transaction.
        /// </summary>
        /// <param name="transactions">The transaction(s) to be transferred over the bus.</param>
        /// <returns>The number of bytes transacted as reported by the I2C hardware.</returns>
        /// <exception cref="IOException">Thrown if a transaction fails and the retry limit is exceeded.</exception>
        public int Execute(params I2CDevice.I2CTransaction[] transactions)
            {
            lock (busMutex)
                {
                EnsureBusConfigured();
                iicBus.Config = configuration;
                int tryCount = 0;
                do
                    {
                    int bytesTransacted = iicBus.Execute(transactions, Timeout);
                    if (bytesTransacted > 0) return bytesTransacted;
                    } while (++tryCount < RetryLimit);
                throw new IOException("I2C transaction failed after " + tryCount + " attempts");
                }
            }

        /// <summary>
        ///     Executes a series of I2C transactions directly from <see cref="ITransactionBuilder" /> instances.
        /// </summary>
        /// <param name="transactions">One or more <see cref="ITransactionBuilder" /> objects containing the transaction data.</param>
        /// <returns>The number of bytes transacted, as reported by the I2C hardware.</returns>
        /// <exception cref="IOException">Thrown if a transaction fails and the retry limit is exceeded.</exception>
        public int Execute(params ITransactionBuilder[] transactions)
            {
            I2CDevice.I2CTransaction[] i2cTransactions = new I2CDevice.I2CTransaction[transactions.Length];
            for (int i = 0; i < transactions.Length; i++)
                {
                i2cTransactions[i] = transactions[i].ToI2CTransaction();
                }
            return Execute(i2cTransactions);
            }

        /// <summary>
        ///     Ensures that the I2C bus is configured exactly once. This prevents the .NetMF I2CDevice class from tripping over
        ///     its own feet.
        /// </summary>
        void EnsureBusConfigured()
            {
            if (iicBus == null)
                {
                iicBus = new I2CDevice(configuration);
                }
            }
        }
    }