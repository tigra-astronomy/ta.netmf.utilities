// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: OutputPortExtensions.cs  Last modified: 2015-03-25@01:23 by Tim Long

using Microsoft.SPOT.Hardware;

namespace TA.NetMF.Utilities.ExtensionMethods
    {
    /// <summary>
    ///     Extension methods for I/O ports.
    /// </summary>
    public static class OutputPortExtensions
        {
        public static void High(this OutputPort port)
            {
            port.Write(true);
            }

        public static void Low(this OutputPort port)
            {
            port.Write(false);
            }
        }
    }