// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: IReadTransactionBuilder.cs  Last modified: 2015-03-24@22:13 by Tim Long

//using JetBrains.Annotations;

namespace TA.NetMF.Utilities.I2C
    {
    public interface IReadTransactionBuilder : ITransactionBuilder
        {
        /// <summary>
        ///     Allocates a read buffer large enough to receive the specified number of bytes.
        /// </summary>
        /// <param name="numberOfBytes">The number of bytes.</param>
        /// <returns>IWriteTransactionBuilder.</returns>
        IReadTransactionBuilder AllocateBuffer(int numberOfBytes);

        /// <summary>
        ///     This will be <c>null</c> until the transaction has been executed, at which point the received data is available here.
        ///     The buffer is cleared each time the builder is converted to an I2C transaction (i.e. when
        ///     <see cref="ITransactionBuilder.ToI2CTransaction" /> is invoked.
        /// </summary>
        /// <value>The received data.</value>
        [CanBeNull]
        byte[] ReceivedData { get; }
        }
    }