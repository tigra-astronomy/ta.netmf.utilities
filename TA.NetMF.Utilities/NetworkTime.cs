// This file is part of the TA.NetMF.Utilities project
// 
// Copyright � 2015 Tigra Astronomy., all rights reserved.
// 
// File: NetworkTime.cs  Last modified: 2015-08-18@00:14 by Tim Long

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.Utilities
    {
    public static class NetworkTime
        {
        /// <summary>
        ///     Gets the Coordinated Universal Time from a network time server.
        ///     Assumes that the network stack is fully initialized.
        /// </summary>
        /// <param name="sntpServer">The SNTP time server. Optional; defaults to "pool.ntp.org".</param>
        /// <returns>
        ///     A <see cref="DateTime" /> with <see cref="DateTimeKind" />set to <see cref="DateTimeKind.Utc" />.
        /// </returns>
        /// <exception cref="System.IO.IOException">Unable to send SNTP request</exception>
        /// <remarks>This code is modified from sample code provided by SecretLabs.</remarks>
        public static DateTime GetNetworkTimeUtc(string sntpServer = "pool.ntp.org")
            {
            Dbg.Trace("Attempting to set the time from " + sntpServer, Source.NetTime);
            IPHostEntry ipHostEntry;
            IPAddress ipAddress;
            IPEndPoint ipEndPoint;

            ipHostEntry = Dns.GetHostEntry(sntpServer);
            ipAddress = ipHostEntry.AddressList[0];
            ipEndPoint = new IPEndPoint(ipAddress, 0x7B);
            Dbg.Trace("Resolved IP Address: " + ipAddress, Source.NetTime);

            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                byte[] dataBuf = new byte[48];
                dataBuf[0] = 0x1B;
                int bytesSent = socket.SendTo(dataBuf, ipEndPoint);
                if (bytesSent != dataBuf.Length)
                    {
                    Dbg.Trace("Failed sending SNTP request.", Source.NetTime);
                    throw new IOException("Unable to send SNTP request");
                    }
                EndPoint remoteEndpoint = ipEndPoint;
                socket.ReceiveFrom(dataBuf, ref remoteEndpoint);
                Dbg.Trace("Network time received", Source.NetTime);
                int index = 40;
                UInt32 seconds = (
                    ((UInt32) dataBuf[index++] << 24) |
                    ((UInt32) dataBuf[index++] << 16) |
                    ((UInt32) dataBuf[index++] << 8) |
                    dataBuf[index++]
                    );
                UInt32 fractionalSeconds = (
                    ((UInt32) dataBuf[index++] << 24) |
                    ((UInt32) dataBuf[index++] << 16) |
                    ((UInt32) dataBuf[index++] << 8) |
                    dataBuf[index++]
                    );
                double fraction = (fractionalSeconds / ((double) (UInt32.MaxValue) + 1));
                var secondsSinceUnixEpoch = Timeout.FromSeconds(seconds + fraction);
                // SNTP's seconds are measured as # of seconds since midnight on January 1, 1900 the Unix Epoch).
                var currentUtcDateTime = DateTime.SpecifyKind(new DateTime(1900, 1, 1), DateTimeKind.Utc);
                currentUtcDateTime += secondsSinceUnixEpoch;
                Dbg.Trace("UTC Time: " + currentUtcDateTime, Source.NetTime);
                return currentUtcDateTime;
                }
            }
        }
    }