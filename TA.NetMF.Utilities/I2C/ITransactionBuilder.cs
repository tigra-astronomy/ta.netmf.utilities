using Microsoft.SPOT.Hardware;

namespace TA.NetMF.Utilities.I2C
    {
    public interface ITransactionBuilder
        {
        /// <summary>
        ///     Converts the accumulated data to an instance of <see cref="I2CDevice.I2CTransaction" />
        /// </summary>
        /// <returns>Microsoft.SPOT.Hardware.I2CDevice.I2CTransaction.</returns>
        I2CDevice.I2CTransaction ToI2CTransaction();

        /// <summary>
        ///     Clears the internal buffer so this instance can be used to build a new transaction.
        /// </summary>
        /// <returns>TA.NetMF.Utilities.I2C.<see cref="TransactionBuilder" />.</returns>
        IWriteTransactionBuilder Clear();
        }
    }